@Regresion
Feature: realizar compras en Pacifika
  Como usuario registrado
  Quiero seleccionar productos realizando un filtro
  Para comprarlos y pagar contraentrega

  @compraVestidoFalda
  Scenario: Compra de 3 prendas femeninas falda-vestido
    Given ingreso a pacifika caso "001"
    When  selecciono productos
    Then  verifico pago contra entrega

  @CompraPrendasHombre
  Scenario: Realizar la compra de diferentes prendas para hombre,
  en intervalos de precio diferentes y uno en rebaja
    Given ingreso a pacifika caso "002"
    When  selecciono productos
    Then  verifico pago contra entrega

  @BuzosYChaquetasHombre
  Scenario: realizar la compra de tres productos en la pagina de
  busos y chaquetas, el  primero,medio y ultimo
    Given ingreso a pacifika caso "003"
    When  selecciono productos
    Then  verifico pago contra entrega

  @PaginasImpares
  Scenario: selecciono productos de home hombres pag impares,
  despues pag 2 y Total no exceder de cien mil
    Given ingreso a pacifika caso "004"
    When  selecciono productos
    Then  verifico pago contra entrega

  @CompraColor
  Scenario: Realizar la compra dependiendo de su color y precio
    Given ingreso a pacifika caso "005"
    When  selecciono productos
    Then  verifico pago contra entrega

  @CompraDesdeHome
  Scenario: seleccionar del home tres prendas con descuento
  y una prenda exclusiva online
    Given ingreso a pacifika caso "006"
    When selecciono productos
    Then verifico pago contra entrega

  @compraRebajas
  Scenario: seleccionar productos de Rebajas y de home hombres
    Given ingreso a pacifika caso "007"
    When selecciono productos
    Then verifico pago contra entrega

  @ComprarDEMIN
  Scenario: Seleccionar el producto mas caro EXCLUSIVO ONLINE
  y seleccionar el producto mas barato con descuento ROJO
    Given ingreso a pacifika caso "008"
    When selecciono productos
    Then verifico pago contra entrega

  @ComprarLUPA
  Scenario: Seleccionar el producto con costo menor de 30000
  seleccionar el producto que sea el resultado de dividir en 2 la cantidad total de prendas encontradas
    Given ingreso a pacifika caso "009"
    When selecciono productos
    Then verifico pago contra entrega