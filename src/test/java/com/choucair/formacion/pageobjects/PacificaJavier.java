package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;


public class PacificaJavier extends PageObject {

    public void carrusel() {
        int valorfinal=0;
        $("//div[@class='banner__component banner']").click();
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("window.scrollBy(0, 1550)");
        int compraEstimada = 0;
        for (int i = 1; i <= 7; i++) {
            String valorprenda;
            String tallas;
            int activo = i + 5;
            String xpathprenda = "html[1]/body[1]/main[1]/div[5]/div[2]/div[2]/div[5]/div[2]/div[1]/div[1]/div[";
            waitFor(xpathprenda+activo+"]/div[1]/a[1]/div[2]/div[1]/h3[1]");
            valorprenda = $(xpathprenda +activo+"]/div[1]/a[1]/div[2]/div[1]/h3[1]").getText();
            String precio = new StringBuilder(valorprenda).replace(0, 1, "").toString();
            int largo = precio.length();
            if (largo == 6) {
                String precio1 = new StringBuilder(precio).replace(2, 2+1, "").toString();
                valorfinal = Integer.parseInt(precio1);
            } else if (largo == 7){
                String precio2 = new StringBuilder(precio).replace(3, 3+1, "").toString();
                valorfinal = Integer.parseInt(precio2);
            }
            if (((valorfinal >= 20000)&&(valorfinal <= 50000))){
                compraEstimada += valorfinal;
                if (compraEstimada > 100000){
                    $("//a[@id='cart-toggle']").click();
                    $("//a[@class='btn btn-primary btn-block mini-cart-checkout-button']").click();
                    break;
                }
                $(xpathprenda+activo+"]/div[1]/a[1]").click();
                tallas = $("//section[@class='col-sm-12 col-md-12 col-lg-12']//ul").getText();
                String[] tallasseparadas = tallas.split("\n");
                if ((tallasseparadas[0].equals("XS"))||(tallasseparadas[0].equals("28"))){
                    $("//li[@class='product-size__lista-item-size list-group-item js-variant-size size-in-stock']//a").click();
                }
                else {
                    $("//ul[@id='variant']//li["+i+"]").click();
                }
                $("//div[@class='overlay-bg visible-overlay']").click();
                $("//div[@class='banner__component banner']").click();
                JavascriptExecutor js = (JavascriptExecutor) getDriver();
                js.executeScript("window.scrollBy(0, 1500)");
            }$("//span[@class='ld-carousel-arrow ld-carousel-arrow-right']").click();}
    }
}


