package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;
import java.util.logging.Logger;

@DefaultUrl("https://www.pacifika.com/")
public class PacifikaHomePage extends PageObject {

    //Ingreso
    @FindBy(xpath = "/html/body/main/nav[2]/div/div[1]/div[3]/ul/li/a")
    WebElementFacade clickRegistroInicioSesion;

    @FindBy(xpath = "//body/main/div/div/section/div/div/div[1]/div[1]/div[1]/div[4]")
    WebElementFacade btnPlusColor;

    @FindBy(xpath = "//*[@id=\"product-facet\"]/div[6]/div[1]/span[3]/div")
    WebElementFacade btnPlusCole;

    @FindBy(xpath = "//*[@id=\"cart-toggle\"]/div[2]/span")
    WebElementFacade txtCar;

    @FindBy(xpath = "//table//a[@class='remove-item-cart'][1]")
    WebElementFacade remove;

    public void clickRegistroInicio() {
        clickRegistroInicioSesion.click();
    }

    public void modalyCookies() {
        WebDriverWait myWaitVar = new WebDriverWait(getDriver(), 3);
        myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.className("mailmunch-popover-iframe")));
        getDriver().switchTo().frame(getDriver().findElement(By.className("mailmunch-popover-iframe")));
        getDriver().findElement(By.xpath("//div[starts-with(@class,'theme')]//a")).click();
        getDriver().switchTo().defaultContent();
        $("//div[@id='js-cookie-notification']//button").click();
    }


    public void seleccionarCategoriaMenu(String p1, String p2, String p3) {
        String xpathCatMenu = "//*[@id=\"navbar\"]/ul/li[";
        String xpathMenucategoria = xpathCatMenu;
        Actions act = new Actions(getDriver());
        act.moveToElement($(xpathMenucategoria + p1 + "]")).perform();
        $(xpathCatMenu + p1 + "]/ul/div/div/ul[" + p2 + "]/li[" + p3 + "]/a").click();
    }

    public void seleccionarCategoriaMenu(String posicion) {
        String xpathCatMenu = "//*[@id=\"navbar\"]/ul/li[";
        $(xpathCatMenu + posicion + "]").click();
    }


    public void seleccionarPrendas(String posicion, String talla) {
        String a = "data-stock";
        Actions actprenda = new Actions(getDriver());
        String xpathCategoriaMenu = "html/body/main/div[6]/div[2]/section/div[1]/div[2]/div[2]/div/div[2]/div[";
        actprenda.moveToElement($(xpathCategoriaMenu + posicion + "]/div[1]/div/a/img")).perform();
        List<WebElement> tallas = getDriver().findElements(By.xpath(xpathCategoriaMenu + posicion + "]/div[1]//section//li//a"));

        int i = 1;
        WebElement liActual;
        for (WebElement element : tallas) {
            liActual = $(xpathCategoriaMenu + posicion + "]/div[1]//section//li[" + i + "]");

            if (element.getText().equals(talla) && (liActual.getAttribute(a).equals("inStock") || liActual.getAttribute(a).equals("lowStock"))) {
            actprenda.moveToElement(element).perform();
                waitFor(element).waitUntilClickable().click();
                waitFor((WebElementFacade) $(xpathCategoriaMenu + posicion + "]/div[1]//section//button")).waitUntilVisible().click();
                break;
            }
            i++;
        }
        i++;
        if (i == (tallas.size() + 2)) {


            i = 1;
            for (WebElement element : tallas) {
                liActual = $(xpathCategoriaMenu + posicion + "]/div[1]//section//li[" + i + "]");

                if ((liActual.getAttribute(a).equals("inStock") || liActual.getAttribute(a).equals("lowStock"))) {
                    actprenda.moveToElement(element).perform();
                    actprenda.moveToElement(element).click().perform();
                    waitFor((WebElementFacade) $(xpathCategoriaMenu + posicion + "]/div[1]//section//button")).waitUntilPresent().click();
                    Logger.getLogger("La talla " + talla + " que usted necesita no se encuentra disponible, fue seleccionada la talla " + element.getText());
                    break;

                }

            }

        }
    }


    public void orden() {
        Actions actorden = new Actions(getDriver());
        actorden.moveToElement($("//*[@id=\"CartContainer\"]/div[2]/div/div/div/div[2]/a")).click().perform();
    }

    public void cerrarOrden() {
        Actions actions = new Actions(getDriver());
        actions.moveToElement($("//a[@id=\"closeCart\"]")).click().perform();
    }


    public void resetNuevaBusqueda() {
        Actions actions = new Actions(getDriver());
        actions.moveToElement($("/html/body/main/nav[2]/div/div[3]/div/div[1]/div/div/a[2]/img")).click().perform();
    }

    public void color(String color) {
        waitFor((WebElementFacade) element(btnPlusColor)).click();
        Actions actprenda = new Actions(getDriver());
        actprenda.moveToElement($("//*[@id=\"facet_facetColor\"]/ul/li[" + color + "]/div/form/div/div/label/div/div[1]")).click().perform();
    }

    public void coleccion(String c) {
        waitFor(btnPlusCole).click();
        Actions actprenda = new Actions(getDriver());
        actprenda.moveToElement($("//*[@id=\"facet_collection\"]/ul/li[" + c + "]/div/form")).click().perform();
    }

    public void vaciarCarro() {
        int a = Integer.parseInt(txtCar.getText());
        if (a != 0) {
            txtCar.click();
            orden();
            for (int i = 1; i <= a; i++) {

                waitFor(remove).waitUntilPresent().click();
                waitFor((WebElementFacade) $("//*[@id=\"removeEntry_0\"]")).waitUntilPresent().click();
            }


        }
    }


}







