package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class PacifikaLoginPage extends PageObject {

    public void iniciarSesion(String correo, String password) {
        find(By.xpath("//input[@id=\"j_username\"]")).type(correo);
        find(By.xpath("//input[@name=\"j_password\"]")).sendKeys(password);
        find(By.xpath("//*[@type=\"submit\"]")).click();
    }


    public void nuevaCuenta() {
        find(By.xpath("//*[@id=\"createAccountButton\"]")).click();    }
}

