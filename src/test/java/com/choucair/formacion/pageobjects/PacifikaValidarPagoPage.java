package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PacifikaValidarPagoPage extends PageObject {
    @FindBy(xpath = "//button[@class='btn btn-primary btn--continue-checkout js-continue-checkout-button']")
    WebElementFacade btnOrdenPagar;
    @FindBy(xpath = "//*[@id=\"register.identificationType\"]/option[2]")
    WebElementFacade lbltipodoc;
    @FindBy(xpath = "//input[@id='register.identificationNumber']")
    WebElementFacade txtnumdocumento;
    @FindBy(xpath = "//input[@id='register.mobileNumber']")
    WebElementFacade txtcelular;
    @FindBy(xpath = "//button[@id='sendBasicRegistration']")
    WebElementFacade btncontinuardatopersona;
    @FindBy(xpath = "//button[@class='btn btn-primary btn-form checkout-next pull-right']")
    WebElementFacade btncontinuaraliado;
    @FindBy(xpath = "//input[@id='address.line1']")
    WebElementFacade txtdireccion;
    @FindBy(xpath = "//select[@id='address-department']")
    WebElementFacade cbxdepartamento;
    @FindBy(xpath = "//select[@id='address-city']")
    WebElementFacade cbxciudad;
    @FindBy(xpath = "//input[@id='address.neighborhood']")
    WebElementFacade txtbarrio;
    @FindBy(xpath = "//input[@id='address-label']")
    WebElementFacade txtdestinatario;
    @FindBy(xpath = "//button[@class='btn btn-primary btn-form checkout-next pull-right change_address_button show_processing_message']")
    WebElementFacade btnirpagar;
    @FindBy(xpath = "//strong[contains(text(),'Pago Contra Entrega')]")
    WebElementFacade lblcontraentrega;

    public void irPagar() {
        btnOrdenPagar.click();
    }

    public void registrarDatosPagoContraEntrega(String dato1, String s, String dato, String doc, String cel) {
            lbltipodoc.click();
            txtnumdocumento.sendKeys(doc);
            txtcelular.sendKeys(cel);
            btncontinuardatopersona.click();
    }

    public void aliados() {
        btncontinuaraliado.click();
    }

    public void datosregistrados() {
        $("//button[@id='sendBasicRegistration']").click();
    }

    public void ubicacionEnvio(String direccion, String departamento, String ciudad, String barrio, String destinatario) {
        List<WebElement> div = getDriver().findElements(By.xpath("//div[@id='i18nAddressForm']/div[2]/div/div[@id='deliveryAddressesSelect']"));
        int count = div.size();
        if (count == 1) {
            waitFor(3).second();
            btnirpagar.click();

        } else if (count == 0){
            waitFor(2).second();
            txtdireccion.sendKeys(direccion);
            cbxdepartamento.sendKeys(departamento);
            waitFor(cbxciudad);
            cbxciudad.click();
            cbxciudad.selectByVisibleText(ciudad);
            txtbarrio.sendKeys(barrio);
            txtdestinatario.sendKeys(destinatario);
            btnirpagar.click();
        }

    }

  public void verificarPago() {
        waitFor(3).second();
        String textoEsperado = "Pago Contra Entrega";
        String mensajeV = lblcontraentrega.getText();
        assertThat(mensajeV, equalTo(textoEsperado));
    }
}

