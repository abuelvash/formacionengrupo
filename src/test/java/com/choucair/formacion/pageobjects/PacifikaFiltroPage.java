package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;


@DefaultUrl("https://www.pacifika.com/moda/vestidos-y-faldas-mujer")
public class PacifikaFiltroPage extends PageObject {
    private int[] posicion = new int[60];
    public int sum = 0;
    private int paginacion;
    PacifikaHomePage pacifikaHomePage;

    public void ordenarpor() {
        find(By.xpath("//select[@id=\"sortOptions1\"]")).click();
        find(By.xpath("//option[@value=\"price-asc\"]")).click();    }

    public int [] filtroprendas(int minimo, int maximo) {
        List<WebElement> divs = getDriver().findElements(By.xpath("//div[@class='ld-prd-item lazy product-list js-ld-hover js-prd-grid-item']"));
        int count = divs.size();
        for (int i = 1; i < count+1; i++) {
            String lista = $("/html[1]/body[1]/main[1]/div[6]/div[2]/section[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[ " + i + "]/div[2]/div[1]/div[1]/h3[1]").getText();
            String separador = new StringBuilder(lista).replace(0, 1, "").toString();
            String separadorfinal = new StringBuilder(separador).replace(2, 3, "").toString();
            String nuevoSeparador = separadorfinal.substring(0, 5);
            int result = Integer.parseInt(nuevoSeparador);
            if (result > minimo && result < maximo) {
                posicion[i] = result;            }
        }
        String total = $("//p[@class=\"search-results-total\"]").getText().substring(0,3);
        Actions actPrenda = new Actions(getDriver());
        int num = (int) Math.ceil((Double.parseDouble(total)) / 51);
        sum = IntStream.of(posicion).sum();
        if (sum==0){    paginacion=paginacion+1;
            for (int i=paginacion; i<num; i++){
                setVar(paginacion);
                String var= String.valueOf(i+1);
                actPrenda.moveToElement($("//div[@class='pagination-bar top']//div[contains(@class,'col-xs-6 col-md-12 search-refine')]//a[@class='pagination_anchor_page_number'][contains(text(),"+var+")]")).click().perform(); }
        }return posicion;
    }

    public int getVar(){
        return sum;    }

    public void setVar(int paginacion){
        this.paginacion =paginacion;    }

    private int contador;
    private int[] prendas;
    public void encontrarposicion() {
        for (int i=0; i<posicion.length;i++){
            if(posicion[i] == 0){ contador =i;
            }else { break; }
        }
        while (ArrayUtils.contains(posicion, 0)) {
            prendas = ArrayUtils.removeElement(posicion, 0);
            break;  }
    }

    public void seleccionar(String talla) {
        for (int i = 0; i < prendas.length; i++) {
            int randomInt = ThreadLocalRandom.current().nextInt(contador, contador + prendas.length);
            pacifikaHomePage.seleccionarPrendas(String.valueOf(randomInt), talla);    }
    }

    public void comprarmasprendas() {
        find(By.xpath("//div[@class=\"overlay-bg visible-overlay\"]")).click();    }

}