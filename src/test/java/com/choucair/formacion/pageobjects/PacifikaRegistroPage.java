package com.choucair.formacion.pageobjects;


import net.serenitybdd.core.pages.PageObject;

public class PacifikaRegistroPage extends PageObject {
    public void registrarsePacifika(String nombre,String apellido,String correo, String contrasena, String sexo){
        $("//input[@id='register.firstName']").sendKeys(nombre);
        $("//input[@id='register.lastName']").sendKeys(apellido);
        $("//input[@id='register.email']").sendKeys(correo);
        $("//input[@id='password']").sendKeys(contrasena);
        $("//input[@id='register.checkPwd']").sendKeys(contrasena);
        if(sexo.trim().equals("F")) {
            $("//label[@for='register.checkPwd1']").click();
        } else
        if(sexo.trim().equals("M")) {
            $("//label[@for='register.checkPwd2']").click();
        }
        $("//button[@type='submit']").click();
    }
}
