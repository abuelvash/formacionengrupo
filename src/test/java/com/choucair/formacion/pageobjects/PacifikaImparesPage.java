package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

public class PacifikaImparesPage extends PageObject {
    PacifikaHomePage pacifikaHomePage;

    @FindBy(xpath = "//a[@id='cart-toggle']")
    WebElementFacade btnbolsacarrito;
    @FindBy(xpath = "//a[@id='cart-toggle']")
    WebElementFacade btnVerOrdenCarrito;

    @FindBy(xpath = "//strong[@class='value']")
    WebElementFacade lblTotalPrecio;

    @FindBy(xpath = "/html/body/main/nav[2]/div/div[1]/div[3]/ul/li[2]")
    WebElementFacade salircuenta;





    public void paginas( String prenda, String talla) {
        int coor = $("//div[@class='pagination-bar top']//div[contains(@class,'col-xs-6 col-md-12 search-refine')]").getCoordinates().onPage().getY();
        Actions actPrenda = new Actions(getDriver());
        String a = (getDriver().findElement(By.xpath("//p[@class='search-results-total']")).getText()).substring(0, 3);
        String xpath1="//div[@class='pagination-bar top']//div[contains(@class,'col-xs-6 col-md-12 search-refine')]//a[@class='pagination_anchor_page_number']";
        int num = (int) Math.ceil((Double.parseDouble(a)) / 51);
        String[] separadorPrenda = prenda.split("#", 3);
        String[] separadorTalla = talla.split("#", 3);
        int result= coor-150;
        for (int i = 1; i <= num; i++) {
            if (i % 2 != 0) {
               if (i == 1) {
                    pacifikaHomePage.seleccionarPrendas(separadorPrenda[0], separadorTalla[0]);
                    waitFor(5).second();
                    pacifikaHomePage.cerrarOrden();
                    waitFor(5).second();
                   JavascriptExecutor jse = (JavascriptExecutor) getDriver();
                   jse.executeScript("window.scrollTo(0,"+result+");");

                } else {
                   actPrenda.moveToElement($(""+ xpath1+" [contains(text(),'"+ i +"')]")).click().perform();
                   JavascriptExecutor jse = (JavascriptExecutor) getDriver();
                   int j=1;
                    pacifikaHomePage.seleccionarPrendas(separadorPrenda[0], separadorTalla[j]);
                    j++;
                    waitFor(5).second();
                    pacifikaHomePage.cerrarOrden();
                    waitFor(5).second();
                   jse.executeScript("window.scrollTo(0,"+result+");");
                   }
            } else {
                actPrenda.moveToElement($(""+ xpath1+" [contains(text(),'"+ i +"')]")).click().perform();
            }
        }
        bolsaCarrito();
    }

    public void bolsaCarrito() {

        waitFor(5).second();
        btnVerOrdenCarrito.click();
        waitFor(5).second();
    }

    public int VerificarPrecioTotal(){
        btnbolsacarrito.click();
        String totalprecio=lblTotalPrecio.getText().replace(".","");
        String precio = totalprecio.replace("$","");
        return Integer.parseInt(String.valueOf(precio));
    }

    public void salir(){
        salircuenta.click();

    }
}