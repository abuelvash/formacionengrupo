package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;

import java.util.List;
import java.util.logging.Logger;

@DefaultUrl("https://www.pacifika.com/")

public class PacifikaLupaPage extends PageObject {

    String xpath1 = "html/body/main/div[5]/div[2]/section/div[1]/div[2]/div/div/div/ul/div[";
    String stock = "data-stock";

    @FindBy(xpath = "/html/body/main/nav[2]/div/div[3]/div/ul/li[1]/a/span")
    WebElementFacade botonLupa;

    @FindBy(xpath = "//*[@id=\"js-site-search-input\"]")
    WebElementFacade barraBusqueda;

    @FindBy(xpath = "/html/body/main/div[2]/div/div/form/div/span[1]")
    WebElementFacade botonBuscar;

    @FindBy(xpath = "/html/body/main/div[5]/div[2]/section/div[1]/div[2]/div/div/div/div[2]/div[7]/div/div/div[1]/div[2]/div[1]/div/ul/li[4]/a")
    WebElementFacade paginaTres;

    public void cajonBusqueda(String busqueda) {
        Actions act = new Actions(getDriver());
        act.moveToElement(barraBusqueda).click().perform();
        barraBusqueda.sendKeys(busqueda);
    }

    public void clickBotonBuscar() {
        Actions act = new Actions(getDriver());
        act.moveToElement(botonBuscar).click().perform();
    }

    public void clickLupa() {
        Actions act = new Actions(getDriver());
        act.moveToElement(botonLupa).click().perform();
    }


    public void clickPaginaTres() {
        Actions act = new Actions(getDriver());
        act.moveToElement(paginaTres).click().perform();
    }

    public void seleccionarPrendaBuscadaEnCajon(String posicion, String talla) {


        Actions actPrenda = new Actions(getDriver());
        actPrenda.moveToElement($(xpath1 + posicion + "]/div[1]/div/a/img")).perform();
        List<WebElement> tallas = getDriver().findElements(By.xpath(" //body//div[" + posicion + "]//div[1]//section[1]//div[1]//form[1]//section[1]//div[1]//div[1]//ul[1]//li//a"));
        int i = 1;
        WebElement liActual;
        for (WebElement element : tallas) {
            liActual = $(" //body//div[" + posicion + "]//div[1]//section[1]//div[1]//form[1]//section[1]//div[1]//div[1]//ul[1]//li[" + i + "]");
            if (element.getText().equals(talla) && (liActual.getAttribute(stock).equals("inStock") || liActual.getAttribute(stock).equals("lowStock"))) {
                actPrenda.moveToElement(element).perform();
                element.click();
                $("/html[1]/body[1]/main[1]/div[5]/div[2]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/ul[1]/div[" + posicion + "]/div[1]/section[1]/div[1]/form[1]/button[1]").click();
                break;
            } else {
                Logger.getLogger("Talla NO Disponible");
            }
            i++;
        }
    }

    public int paginas() {
        Actions actPrenda = new Actions(getDriver());
        String a = (getDriver().findElement(By.xpath("//p[@class='search-results-total']")).getText()).substring(0, 3);
        Logger.getLogger(a);
        double numpag = ((Double.parseDouble(a)) / 2) / 51;
        int num = (int) Math.ceil(numpag);
        float dec = (float) (numpag % 1);
        int numele = (int) (dec * 51);
        for (int i = 2; i <= num; i++) {
            actPrenda.moveToElement($("//div[@class='pagination-bar top']//div[contains(@class,'col-xs-6 col-md-12 search-refine')]//a[@class='pagination_anchor_page_number'][contains(text(),'" + i + "')]")).click().perform();
        }
        return numele;
    }


    public void seleccionarPrendaslupa(int posicion, String talla) {
        Actions actPrenda = new Actions(getDriver());
        actPrenda.moveToElement($(xpath1 + posicion + "]/div[1]/div/a/img")).perform();
        List<WebElement> tallas = getDriver().findElements(By.xpath("//ul[@class='product__listing product__grid ld-product-grid']/div[" + posicion + "]/div[1]//section//li//a"));

        int i = 1;
        WebElement liActual;
        for (WebElement element : tallas) {
            liActual = $(xpath1 + posicion + "]//*[@id='command']/section/div/div/ul/li[" + i + "]");

            if (element.getText().equals(talla) && (liActual.getAttribute(stock).equals("inStock") || liActual.getAttribute(stock).equals("lowStock"))) {
                actPrenda.moveToElement(element).perform();
                element.click();
                $("/html[1]/body[1]/main[1]/div[5]/div[2]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/ul[1]/div[" + posicion + "]/div[1]/section[1]/div[1]/form[1]/button[1]").click();
                break;
            } else {
                Logger.getLogger("Talla NO Disponible");
            }
            i++;
        }
    }
}


