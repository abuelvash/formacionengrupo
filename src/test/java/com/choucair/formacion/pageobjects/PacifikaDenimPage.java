package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

public class PacifikaDenimPage extends PageObject {
    @FindBy(xpath = "//a[@class='btn btn-primary btn-block mini-cart-checkout-button']")
    WebElementFacade verord;

    public boolean escojerPrenda(String posicion, String talla) {
        Actions actions;
        String raiz = "/html/body/main/div[6]/div[2]/section/div[1]/div[2]/div[2]/div/div[2]/div[";
        actions = new Actions(getDriver());
        boolean bandera = true;
        actions.moveToElement($(raiz + posicion + "]/div[1]/div/a/img")).perform();
        List<WebElement> tallas = getDriver().findElements(By.xpath(raiz + posicion + "]/div[1]//section//li//a"));
        int i = 1;
        WebElement liActual;
        for (WebElement element : tallas) {
            liActual = $(raiz + posicion + "]/div[1]//section//li[" + i + "]");

            if (element.getText().equals(talla) && (liActual.getAttribute("data-stock").equals("inStock") || liActual.getAttribute("data-stock").equals("lowStock"))) {
                actions.moveToElement(element).perform();
                element.click();
                $(raiz + posicion + "]/div[1]//section//button").click();
                break;
            } else {
                bandera = false;
            }
            i++;
        }
        return bandera;
    }


    public void PrendaMasCaraExclusiva() {
        $("//select[@id='sortOptions1']//option[4]").click();
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("window.scrollBy(0, 600)");
        List<WebElement> divs = getDriver().findElements(By.xpath("//div[starts-with(@class,'ld-prd-item lazy')]"));
        int count = divs.size();
        for (int i = 1; i <= count; i++) {
            String exclusiva;
            exclusiva = $("//div[starts-with(@class,'product__listing')]/child::div[" + i + "]").getTextValue();
            String[] separado = exclusiva.split("\n");
            if (separado[0].equals("Exclusivo Online")) {
                escojerPrenda("" + i, "M");
                $("//div[@class='overlay-bg visible-overlay']").click();
                return;
            }
        }
    }

    public void baratoConDescuento() {
        $("//select[@id='sortOptions1']//option[3]").click();
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("window.scrollBy(0, 600)");
        List<WebElement> divs = getDriver().findElements(By.xpath("//div[starts-with(@class,'ld-prd-item lazy')]"));
        int count = divs.size();
        for (int i = 1; i <= count; i++) {
            String barato;
            barato = $("//div[starts-with(@class,'product__listing')]/child::div[" + i + "]").getTextValue();
            String[] separado = barato.split("\n");
            if (separado[0].startsWith("-")) {
                escojerPrenda("" + i, "10");
                verord.click();
                return;
            }
        }
    }

    }



