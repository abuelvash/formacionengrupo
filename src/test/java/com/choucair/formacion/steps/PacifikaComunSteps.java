package com.choucair.formacion.steps;

import au.com.bytecode.opencsv.CSVReader;
import com.choucair.formacion.pageobjects.*;
import net.thucydides.core.annotations.Step;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;



public class PacifikaComunSteps {

    private static String[]datos;

    PacifikaHomePage pacifikaHomePage;
    PacifikaLoginPage pacifikaLoginPage;
    PacifikaRegistroPage pacifikaRegistroPage;
    PacifikaValidarPagoPage pacifikaValidarPagoPage;
    PacifikaDenimPage pacifikaDenimPage;
    PacifikaFiltroPage pacifikaFiltroPage;
    PacifikaImparesPage pacifikaImparesPage;
    PacifikaLupaPage pacifikaLupaPage;
    PacificaJavier pacificaJavier;

    public static void leerCSV(String casoPrueba){
        CSVReader reader;
        try {

            reader = new CSVReader(new FileReader("src/test/resources/Datadriven/Datadriven.csv"));
            String[] fila;
            while ((fila = reader.readNext())!=null){

                Logger.getLogger(fila[0]);

                if (casoPrueba.equals(fila[0].trim())){
                    datos = fila;
                }
            }

            reader.close();

        } catch (IOException e){
            Logger.getLogger(""+e);
        }
    }

    @Step
    public void ingresoPacifika(String casoPrueba) {

        leerCSV(casoPrueba);
        String estado = datos[17];


        pacifikaHomePage.open();
        pacifikaHomePage.modalyCookies();
        pacifikaHomePage.clickRegistroInicio();

        if (estado.equals("sinregistro")){

            pacifikaLoginPage.nuevaCuenta();
            pacifikaRegistroPage.registrarsePacifika(datos[3], datos[4], datos[1], datos[2], datos[8]);

        }else {

           pacifikaLoginPage.iniciarSesion(datos[1], datos[2]);

        }

    }

    @Step
    public void seleccionarProductos() {

        String caso = datos[0];

        if (caso.equals("001")) {
            int minimo=30000; int maximo=50000;
            pacifikaHomePage.seleccionarCategoriaMenu("1", "2", "1");
            pacifikaFiltroPage.ordenarpor();
            pacifikaFiltroPage.filtroprendas(minimo,maximo);

            busqueda:
            if (pacifikaFiltroPage.sum != 0){
                pacifikaFiltroPage.encontrarposicion();
                String talla = datos[16];
                String[] separadorTalla = talla.split("#", 3);
                for (int i=0; i<separadorTalla.length; i++){
                    pacifikaFiltroPage.seleccionar(separadorTalla[i]);
                    if (i != separadorTalla.length ) {
                        pacifikaFiltroPage.comprarmasprendas();  }
                }
                pacifikaHomePage.orden();}

        } else if (caso.equals("002")) {
            pacifikaHomePage.vaciarCarro();
            String categoria;
            String prenda;
            String talla;
            categoria = datos[14];
            prenda = datos[15];
            talla = datos[16];

            String[] separadorCategoria = categoria.split("#", 4);
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[0], separadorCategoria[1], separadorCategoria[2]);
            String[] separadorPrenda = prenda.split("#", 3);
            String[] separadorTalla = talla.split("#", 3);
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[0], separadorTalla[0]);
            cerrarOrdenCarrito();
            pacifikaHomePage.resetNuevaBusqueda();
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[0], separadorCategoria[1], separadorCategoria[2]);
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[1], separadorTalla[1]);
            cerrarOrdenCarrito();
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[3]);
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[2], separadorTalla[2]);
            verOrdenCarrito();

        } else if (caso.equals("004")) {
            String categoria;
            categoria = datos[14];
            String[] separadorCategoria = categoria.split("#", 4);
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[0]);
            pacifikaImparesPage.paginas(datos[15], datos[16]);
            if ((pacifikaImparesPage.VerificarPrecioTotal()) <= 100000) {
                verOrdenCarrito();
                Logger.getLogger("ingreso al carrito");
            } else {
                Logger.getLogger("Su saldo es mayor a 100.000");
                pacifikaHomePage.cerrarOrden();
                pacifikaImparesPage.salir();
            }

        } else if (caso.equals("005")) {

            String categoria;
            String prenda;
            String talla;
            categoria = datos[14];
            prenda = datos[15];
            talla = datos[16];
            pacifikaHomePage.vaciarCarro();
            String[] separadorCategoria = categoria.split("#", 1);
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[0]);
            pacifikaHomePage.color("3");
            pacifikaHomePage.coleccion("2");
            String[] separadorPrenda = prenda.split("#", 4);
            String[] separadorTalla = talla.split("#", 4);
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[0], separadorTalla[0]);
            cerrarOrdenCarrito();
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[1], separadorTalla[1]);
            cerrarOrdenCarrito();
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[2], separadorTalla[2]);
            cerrarOrdenCarrito();
            pacifikaHomePage.seleccionarCategoriaMenu(separadorCategoria[0]);
            pacifikaHomePage.color("2");
            pacifikaHomePage.coleccion("4");
            pacifikaHomePage.seleccionarPrendas(separadorPrenda[3], separadorTalla[3]);
            verOrdenCarrito();

        } else if (caso.equals("006")) {
            pacifikaHomePage.vaciarCarro();
            pacificaJavier.carrusel();

        }
        else if (caso.equals("008")) {
            String categoria;
            categoria = datos[14];
            pacifikaHomePage.vaciarCarro();
            pacifikaHomePage.seleccionarCategoriaMenu(categoria);
            pacifikaDenimPage.PrendaMasCaraExclusiva();
            pacifikaDenimPage.baratoConDescuento();
        }


        else if (caso.equals("009")) {

            pacifikaHomePage.vaciarCarro();
            pacifikaLupaPage.clickLupa();
            pacifikaLupaPage.cajonBusqueda(datos[15]);
            pacifikaLupaPage.clickBotonBuscar();
            pacifikaLupaPage.clickPaginaTres();
            pacifikaLupaPage.seleccionarPrendaBuscadaEnCajon(datos[14], datos[16]);
            pacifikaLupaPage.clickLupa();
            pacifikaLupaPage.cajonBusqueda(datos[15]);
            pacifikaLupaPage.clickBotonBuscar();
            int pag = pacifikaLupaPage.paginas();
            pacifikaLupaPage.seleccionarPrendaslupa(pag, datos[16]);
            verOrdenCarrito();


        }
    }

    @Step
    public void verificoPagoContraEntrega(){
        String estado = datos[17];
        pacifikaValidarPagoPage.irPagar();
        if (estado.equals("registro")){
            pacifikaValidarPagoPage.datosregistrados();
        }else {
            pacifikaValidarPagoPage.registrarDatosPagoContraEntrega(datos[5],datos[3],datos[4],datos[6],datos[7]);
        }
    pacifikaValidarPagoPage.aliados();
    pacifikaValidarPagoPage.ubicacionEnvio(datos[9],datos[10],datos[11],datos[12],datos[13]);
    pacifikaValidarPagoPage.verificarPago();
    }

    @Step
    public void cerrarOrdenCarrito() {
        pacifikaHomePage.cerrarOrden();
    }
    @Step
    public void verOrdenCarrito(){
        pacifikaHomePage.orden();
    }
}
