package com.choucair.formacion.definition;


import com.choucair.formacion.steps.PacifikaComunSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PacifikaComunDefinition {

    String casoPrueba;


    @Steps
    PacifikaComunSteps pacifikaComunSteps;

   @Given("^ingreso a pacifika caso \"([^\"]*)\"$")
    public void ingresoAPacifikaCaso(String idCaso){
        this.casoPrueba = idCaso;
        pacifikaComunSteps.ingresoPacifika(idCaso);
    }

    @When("^selecciono productos$")
    public void seleccionoProductos() {
        pacifikaComunSteps.seleccionarProductos();
    }

    @Then("^verifico pago contra entrega$")
    public void verificoPagoContraEntrega() {
        pacifikaComunSteps.verificoPagoContraEntrega();

    }

}
